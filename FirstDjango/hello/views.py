from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.models import User
from django.http import HttpRequest, HttpResponse
from django.template.loader import render_to_string
from django.template import loader, Template
from hello.models import Publisher
import datetime
from hello.forms import PublisherForm


# Create your views here.
# 定义业务请求处理的函数
def hello(request, a):
    # print(a)
    # print(request.method)

    print(request.POST.get('key'))
    user_list = User.objects.all()  # 从用户模型中查询所有的用户,查询内容保存到变量里面
    # print(user_list.query)
    print(user_list)

    # 1.不好用
    # t = loader.get_template('table.html')
    # c = {'user_list': user_list}
    # return HttpResponse(t.render(c, request), content_type='text/html')

    # print(locals())

    athlete_list = [1, 2, 3, 4, 5, 6]
    return render(request, 'table.html', locals())

    # locals把当前作用域的变量一起传递给模版变量,无论自己或系统定义的。是以字典的方式传过去
    # return redirect('/test/22/dd') # 跳转页面
    # return render_to_response('table.html', {'user_list': user_list}) # 2.提交就不好用了

    # response = HttpResponse("Here's the text of the web page.")
    # return response


def test(request):
    athlete_list1 = [1, 2, 3, 4, 5, 6]
    ver = 'abc'
    value = datetime.datetime.now()
    value1 = [1, 2, 3, 4]
    value2 = "<a href=''>百度</a>"
    value3 = 1234.56
    value4 = "https://www.example.org/foo?a=b&c=d"
    value5 = "Joel is a slug"
    value6 = Template("<a href='http://www.biying.com' target='_blank'>必应</a>")
    aaaaaaaaaaaaaaaa = 'I Love You'
    # return render(request, 'table.html', locals())
    return render(request, 'a.html', locals())


def add_publisher(request):
    # 不使用Form的情况
    # if request.method == 'POST':
    # # 如果提交为post,去接收用户提交过来的数据
    #     # print(request.POST)
    #     name = request.POST['name']
    #     address = request.POST.get('address')
    #     city = request.POST['city']
    #     state_province = request.POST['state_province']
    #     country = request.POST['country']
    #     website = request.POST.get('website')
    #
    #     if name or address or city or state_province or country or website == '':
    #         return HttpResponse('添加信息不能为空!')
    #     else:
    #         Publisher.objects.create(
    #             name=name,
    #             address=address,
    #             city=city,
    #             state_province=state_province,
    #             country=country,
    #             website=website,
    #         )
    #         return HttpResponse('添加出版社信息成功!')
    # else:
    #     return render(request, 'add_publisher.html', locals())

    # # 使用Django Form的情况
    # if request.method == 'POST':
    #     publisher_form = PublisherForm(request.POST)
    #     if publisher_form.is_valid():
    #         Publisher.objects.create(
    #             name=publisher_form.cleaned_data['name'],
    #             address=publisher_form.cleaned_data['address'],
    #             city=publisher_form.cleaned_data['city'],
    #             state_province=publisher_form.cleaned_data['state_province'],
    #             country=publisher_form.cleaned_data['country'],
    #             website=publisher_form.cleaned_data['website'],
    #         )
    #         return HttpResponse('添加出版社信息成功!')

    # 使用Django ModelForm的情况
    if request.method == 'POST':
        publisher_form = PublisherForm(request.POST)
        if publisher_form.is_valid():
            publisher_form.save()   # 这个方法根据表单绑定的数据创建并保存数据库对象。
            return HttpResponse('添加出版社信息成功!')
    else:
        publisher_form = PublisherForm()

    return render(request, 'add_publisher.html', locals())
